#include <igloo/igloo_alt.h>

#include <algorithm>
#include <vector>

#include <iostream>

class Same {
public :
    static bool comp(std::vector<int>& a, std::vector<int>& b) {
        std::cout << "\na : ";
        print(a);
        std::cout << "b : ";
        print(b);

        if (a.empty() && b.empty()) {
            return true;
        }
        else if (a.empty() || b.empty() || (a.size() != b.size())) {
            return false;
        }
      
        std::sort(a.begin(), a.end(),
                  [](int first, int second) {
                      return std::abs(first) < std::abs(second);
                  });
        std::sort(b.begin(), b.end());

        std::cout << "sa: ";
        print(a);
        std::cout << "sq: ";
        printSquared(a);
      
        std::cout << "sb: ";
        print(b);

        auto it_a = a.begin();
        auto it_b = b.begin();

        for (; it_a != a.end(); ++it_a, ++it_b) {
            if ((*it_a) * (*it_a) != *it_b) {
                std::cout << "False returned!" << std::endl;
                std::cout << "----------------------------------------------------\n";
                return false;
            }
        }
        std::cout << "True returned!" << std::endl;
        std::cout << "----------------------------------------------------\n";
        return true;
    }
  
    static void print(const std::vector<int>& vec) {
        for (auto el : vec) {
            std::cout << el << ' ';
        }
        std::cout << '\n';
    }
  
    static void printSquared(const std::vector<int>& vec) {
        for (auto el : vec) {
            std::cout << el * el << ' ';
        }
        std::cout << '\n';
    }
};

int main(int argc, char *argv[]) {
  return igloo::TestRunner::RunAllTests(argc, argv);
}
