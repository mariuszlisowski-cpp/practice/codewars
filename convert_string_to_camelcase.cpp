#include <algorithm>
#include <cctype>
#include <iostream>
#include <regex>
#include <string>

std::string to_camel_case(std::string text) {
    std::string camel{};
    camel.reserve(text.length());
    bool isUpper = false;
    for(char c : text) {
        if (c == '_' || c == '-') {
            isUpper = true;
        } else if (isUpper) {
            camel += static_cast<char>(std::toupper(c));
            isUpper = false;
        } else {
            camel += c;
        }
    }

    return camel;
}

// codewars
std::string to_camel_case_short(std::string text) {
    for(int i{0}; i < text.size(); ++i) {
        if(text[i] == '-' || text[i] == '_')
            text.erase(i, 1), text[i] = std::toupper(text[i]);
    }

    return text;
}

int main() {
    std::string str{"the_stealth_warrior_"};

    std::cout << to_camel_case(str) << std::endl;
    std::cout << to_camel_case_short(str) << std::endl;

    for(int i = 0, j = 0; i < 5 && j < 10; j++, i < 4 ? i++ : i) {
        std::cout << i << " " << j << std::endl;
    }

    return 0;
}
