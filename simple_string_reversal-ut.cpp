#include <gtest/gtest.h>

#include "simple_string_reversal.cpp"

TEST(ReverseWithSpaces, NoSpacesOneLoop) {
    ASSERT_EQ("srawedoc", reverseOneLoop("codewars"));
}

TEST(ReverseWithSpaces, WithSpacesOneLoop) {
    ASSERT_EQ("edoc ruoy", reverseOneLoop("your code"));
    ASSERT_EQ("skco redo cruoy", reverseOneLoop("your code rocks"));
    ASSERT_EQ("8765 4321", reverseOneLoop("1234 5678"));
    ASSERT_EQ("EDCB A876 54321", reverseOneLoop("1234 5678 ABCDE"));
}

TEST(ReverseWithSpaces, NoSpacesTwoLoops) {
    ASSERT_EQ("srawedoc", reverseTwoLoops("codewars"));
}

TEST(ReverseWithSpaces, WithSpacesTwoLoops) {
    ASSERT_EQ("edoc ruoy", reverseTwoLoops("your code"));
    ASSERT_EQ("skco redo cruoy", reverseTwoLoops("your code rocks"));
    ASSERT_EQ("8765 4321", reverseTwoLoops("1234 5678"));
    ASSERT_EQ("EDCB A876 54321", reverseTwoLoops("1234 5678 ABCDE"));
}
